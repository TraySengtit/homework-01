import React from 'react'
import { View, StyleSheet, SafeAreaView, StatusBar } from 'react-native'

const App = () => {
  return (
    <View style={[styles.container, {
      flexDirection: "column"
    }]}>
      {
        Platform.OS === 'ios' ?
          <SafeAreaView backgroundColor="#7fff00"/> :
          <StatusBar backgroundColor="#7fff00"/>
      }

      <View style={styles.topBar}>
      <View style={styles.backGroundBox} >
        <View style={styles.boxCover}>
        <View style={styles.circle}>
          <View style={styles.box}/>
        </View>
        <View style={styles.wrapRectangle}>
          <View style={styles.rectangle}/>
          <View style={[styles.rectangle1]}/>
        </View>
        </View> 
      </View>
      <View style={{width: '100%', height: '25.5%', flexDirection: "row", backgroundColor: "darkorange"}}>
        <View style={{backgroundColor:"gray",width:"33.33%",height:'100%'}}>
          <View style={{backgroundColor: '#7FFF00', width: '100%', height: '50%'}}></View>
          <View style={{backgroundColor: "#9400d3", width: '100%', height: '50%'}}></View>
        </View>
        <View style={{backgroundColor:"black",width:"33.33%",height:'100%', flexDirection: 'row'}}>
          <View style={{backgroundColor: "yellow", width: '50%', height: '100%'}}></View>
          <View style={{backgroundColor: "plum", width: '50%', height: '100%'}}></View>
        </View>
        <View style={{backgroundColor:"gray",width:"33.33%",height:'100%'}}>
          <View style={{backgroundColor: "snow", width: '100%', height: '50%'}}></View>
          <View style={{backgroundColor: "red", width: '100%', height: '50%'}}>
            <View style={{backgroundColor:"black",width:"100%",height:'100%', flexDirection: 'row'}}>
              <View style={{backgroundColor: "red", width: '50%', height: '100%'}}></View>
              <View style={{backgroundColor: "mediumspringgreen", width: '50%', height: '100%'}}></View>
            </View>
          </View>
        </View>
      </View>
      </View>
      <View style={{width: '100%', height: '35%', backgroundColor: "dodgerblue", flexWrap: 'wrap', justifyContent: 'space-around', alignContent: 'space-around'}}>
        <View style={styles.sixBox}></View>
        <View style={styles.sixBox}></View>
        <View style={styles.sixBox}></View>
        <View style={styles.sixBox}></View>
        <View style={styles.sixBox}></View>
        <View style={styles.sixBox}></View>
      </View>
      <View style={{width: '100%', height: '15%', backgroundColor: "mediumpurple", flexWrap: 'nowrap', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
        <View style={styles.circleBottom}></View>
        <View style={styles.circleBottom}></View>
        <View style={styles.circleBottom}></View>
        <View style={styles.circleBottom}></View>
      </View>
    </View>

  )
}
const styles = StyleSheet.create({
  container: {
    height: '100%',
    padding: 0,
  },
  circleBottom: {
    width: 55,
    height: 55,
    borderRadius: 50,
    backgroundColor: 'white'
  },
  sixBox:{
    width: '29%',
    height: '42%',
    backgroundColor: 'floralwhite',
  },
  topBar:{
    width: '100%',
    height: '50%',
    backgroundColor: 'yellow'
  },
  backGroundBox:{
    alignItems:'center',
    justifyContent:'center',
    width: '100%', 
    height: '70%', 
    backgroundColor: "dodgerblue", 
    alignItems: 'center', 
    marginTop: '3%', 
  
  },
  boxCover:{
    // backgroundColor: 'orange',
    width: '57%',
    height: '65%',
    justifyContent:'space-between',
    alignItems: 'center', 
  },
  circle: {
    width: 69,
    height: 69,
    backgroundColor:'white',
    margin:'auto',
    // marginTop:30,
    // top: 30,
    borderRadius: 50,
    alignItems:'center',
    justifyContent:'center'
  },
  box:{
    width:42,
    height:42,
    backgroundColor:'blue'
  },
  wrapRectangle:{
    width: '100%',
    height: '47%',
    alignItems:'flex-end',
    justifyContent:'space-between'
  },
  rectangle:{
    width:'100%',
    height:'40%',
    backgroundColor:'white',
  },
  rectangle1:{
    width:'50%',
    height:'40%',
    backgroundColor:'white',    
  }
});
export default App
